Un día más nos reunimos en el estudio para dar a conocer las novedades de Gueden... Las novedades de verdad. Al habla Heige, desde algún lugar de Kharide. Recordamos a les oyentes que hablamos sin el beneplácito del rey Argaven Harge (manteneros a la escucha bajo vuestra responsabilidad), como siempre decimos: confía en el rey o confía en nosotres. Como ya sabréis hay algunas novedades relacionadas con la visita del nuevo primer ministro Pemmer Harge rem ir Tibe al norte del valle de Sinod. Pero no es eso de lo que queremos hablar hoy: traemos algo más peculiar.
[MUSICA] 

El misterioso Enviado que se hace llamar "Ai", y que hasta hace poco contaba con el apoyo del exministro Estraven, trajo consigo material y tecnología hasta ahora desconocidas. Une de les científiques que era responsable del análisis de ese material, nos ha contado en absoluta confidencia los detalles de uno de sus aparatos más sorprendentes: el ansible. Este enviado es un ser que se mantiene en constante Kemmer (¿os imaginais?), pero lo mas sorprendente no es que él sea así, si no que según dice, toda su especie y otras de la alianza Ecumen a la que representa son así. Nos preguntamos cómo una sociedad en constante Kemmer ha sido capaz de desarrollar está nueva y potente tecnología.
[MUSICA]

El ansible permite transmitir mensajes lejos, en el Vacío. Cuando algo se escribe en el ansible, también es escrito, simultáneamente en alguna parte muy lejos de éste. Sabemos que no se tratan de ondas de radio, es otra cosa. Por supuesto hace falta tener soltura con la lengua del Vacío, la lengua del Ecumen. Pero nuestra curiosidad llega hasta esos extremos y desde aquí anunciamos, que algunes estamos dispuestes a aprender dicha lengua, y la cultura de los que son permanentemente Kémmer, si con ello podemos aprovechar la tecnología del Ansible.
[MUSICA]

Deseamos pues que Ai pueda compartir algo más de este extraño conocimiento.
Gracias por escuchar, y nos vemos pronto. 
