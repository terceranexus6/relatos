
El movimiento del agua era tranquilizador. Las olas comenzaban y rompían en el cubículo generando una extraña armonía. Las paredes negras del cubículo daban una extraña sensación de vacío pero el agua, ajena a esta sensación e impasible, continuaba su pausado ritmo. "casi como un haiku" pensaba Ahmya (アーミャ) con la nariz estrujada sobre la cristalera que separaba el oscuro cubículo de la biblioteca. 

Una hoja resbalo por sus dedos y recordó que tenía un libro entre las manos. Por acto reflejo intentó que la hoja por donde había dejado su lectura no terminase de caer, pero el resultado fue que el libro se escapó de sus manos y cayó al suelo, sobresaltando a lectoras desprevenidas enfrente suya. Recogió el libro del suelo con un silencioso movimiento de cabeza a modo de disculpa y cuando las afectadas retomaron su lectura, Ahmya dedicó un instante a observar la portada de su libro. 

"Plantas del antiguo y del nuevo mundo"

Su cabeza había volado imaginando como eran las hojas silvestres de un mundo lejano y antiguo, aunque no recordaba en qué momento exactamente esos pensamientos se habían volcado sobre las olas sintéticas del cubículo al que da la ventana de la sala. Apretó el libro entre sus manos, como quien se agarra a un pedazo de realidad para no salir volando, y salió de la biblioteca. En la entresala que comunicaba con varios pasillos a la salida de la biblioteca había un gran panel táctil, apagado. Cogió uno de los paños que cuidadosamente había en un cesto al lado del monitor, limpió un poco los restos de huellas, y encendió el monitor justo después de lanzar el trapo a la cesta. 

El panel se encendió lentamente mostrando las tareas pendientes del edificio. Acercó su pulsera al lector lateral y algunas tareas parpadearon en tono verdoso, las tareas recomendadas para ella ese día. Echó un vistazo rápido mientras algunas personas cruzaban la entresala sin prestarle atención. Las tareas señaladas en verde eran "cuidado del jardín para la escuela - de 11:00 a 13:00", "limpieza de las salas 15 y 8 - de 12:00 a 14:30", "asistencia en el laboratorio de radio - de 17:00 a 17:45" y "colaboración en la sala de debate de nivel 3 - de 16:00 a 18:00". Miró su reloj en la pulsera, aún era temprano. Decidió cuidar del jardín primero y asistir al debate de nivel 3 (con estudiantes de 15 y 16 años) que siempre le resultaban distraídas. A esa edad empezaban a comprometerse con las más variadas ocurrencias sin perder la curiosidad de los primeros grupos, más jóvenes. Ella sólo iría para ayudar a guiar el debate y plantear cuestiones sobre las que trabajar. A sus 25 años, Ahmya aún recordaba con ilusión esas charlas, que después se alargaban en privado. Señaló las tareas en el monitor y echó un ojo a las tareas no recomendadas, ya que no tenía prisa. Alguna vez había seleccionado una de esas tareas, puesto que la recomendación era automática, pero por seguir un orden solía aceptar recomendaciones. Había una tarea por la noche, el título era poco intuitivo: "testeo de actualizaciones sobre el sistema automático de guiado personal - de 21:00 a 22:30".
De nuevo cogió un paño y limpió suavemente los restos de polvo y huellas del monitor antes de marcharse al jardín. 

Aunque en su lectura matinal el mundo de las plantas y el cuidado era romántico e idílico, la realidad era transparente y práctica. A la media hora de comenzar su trabajo su peto de trabajo ya estaba lleno de manchurrones y ramitas, se había dedicado a preparar la tierra y las cestas de semillas para el huerto infantil, que las niñas aprenderían a utilizar esa misma tarde para aprender posteriormente a cuidar de los huertos comunes, como parte de sus estudios ambientales. Al rato, mientras Ahmya seleccionaba cuidadosamente las semillas y materiales para los kits de estudiantes, Cyra (سایرا) llegó al jardín con cajas del laboratorio de electrónica ambiental. La chica había compartido con ella muchos de los debates de grupo que antes había recordado. Tenía una naturaleza mucho más impulsiva que Ahmya pero siempre habían encontrado un punto de equilibrio. Cyra pasaba mucho tiempo en tareas de electrónica y comunicación desde muy joven, casi exclusivamente, así que había asentado su vida en esta disciplina. Se sonrieron mutuamente antes de que la chica se sentase al lado de Ahmya mientras preparaba las semillas.

_ ¿Qué van a plantar esta vez? _  preguntó con curiosidad ojeando uno de los kits ya terminados.


_ Zanahorias, planta de mostaza y  espinacas. Son bastante resistentes, ¡van a evitar muchas frustraciones!_ respondio Ahmya sonriendo, pensando en los pisotones y despistes infantiles. 


_ Igual yo debería apuntarme a esa clase también. _ declaró Cyra mirando la tierra blanda preparada. _  Bueno, te traigo el complemento para la clase posterior. 

Sacó de su caja un sensor con forma de flecha.

_ Tendrán que instalar estos sensores al lado de las plantas para monitorizarlas desde la sala de electrónica. _ Le entregó el sensor. _ Y después tendrán que conectarlo a este microprocesador... y esta placa solar._ Le entrego ambas cosas. Ahmya tenía el regazo lleno entre los aparatos y la cestita que estaba preparando.


_ ¿Qué hago con esto?


_ Pues no se si deberías mezclarlo con los kits o ponerlo a parte. A parte mejor, tienes razón. _ Tal y como se lo había entregado se lo volvió a quitar y a meterlo en la caja. Cyra siempre era así, tan voluble que le resultaba confuso. Aunque había aprendido a apreciar esta forma de ser y de hecho si alguna vez no la encontraba en sus conversaciones, sabía que algo iba mal. 


_ Después voy a colaborar en el debate de nivel 3. ¿Recuerdas cuando nos tocaba a nosotras? _ mencionó Ahmya mientras obserbaba a la chica organizar los sensores en su caja. Cyra se rió suavemente sin dejar de mirar dentro de la caja.


_ Recuerdo aquella sobre... Sobre ¿matemáticas? bueno sobre lógica. Carlos se había aprendido de memoría un acertijo en la biblioteca.


_ ¡Lo dijo al revés! _ rió Ahmya.


_ Lo dijo al revés y el coordinador no sabía si no sabía resolverlo o si lo estaba diciendo mal. _ soltó la caja gesticulando. _ Pero fue interesante. Me gustaban esos ratos. Carlos también pasa mucho rato en el laboratorio de electrónica ambiental, y el de sistemas internos. Deberías venir un día, podríamos hablar de esas cosas.


Cyra se levantó dejando la caja en el suelo.

_ Ya está listo. ¿Quieres comer luego algo? Estaré libre a las 14:00.


_ Claro, nos vemos luego._ Respondió Ahmya. El silencio repentino de la sala tras escuchar el eco de los pasos de Cyra en el pasillo le hizo volver a perderse en sus pensamientos un rato más. Cuando se dió cuenta continuó trabajando. 

Terminó un poco más tarde de lo previsto, pero no llegó tarde a su cita con Cyra. La esperó pacientemente frente a la puerta del laboratorio de electrónica ambiental, del cual salió charlando con dos compañeros, uno de ellos era Carlos.


_ Ahmya, cuánto tiempo._ Dijo él, mientras se despedía del tercer compañero. _ ¿En qué has estado trabajando estos días?


_ No me he centrado en nada en concreto. _ aclaró Ahmya. _ Aunque he estado investigando sobre plantas, quizás me vuelque más en el cuidado de jardines.


_ Viene inspirada de su trabajo matinal. _ añadió Cyra, que estaba terminando de despedirse de compañeras antes de volver a la conversación.


_ ¿Creéis que las plantas estarán  mejor cuidadas con los sensores?_ preguntó Ahmya mientras caminaban a la cafetería, a través de un gran pasillo acristalado que dejaba entrever parte de la fachada y el cielo rojizo. 


_ ¿A qué te refieres? _ Carlos parecía confuso. _ Si no fueran útiles no se enseñaría, ¿no?


_ Quiero decir... _ Ahmya escogió cuidadosamente sus palabras. _ Antes no se usaban para nada, ¿no? según estaba leyendo, y parecía que les iba muy bien. Sólo me pregunto si es necesario. ¿Sabéis? 


_ Los materiales y toda la infraestructura están pensados para no dañar ni afectar a las plantas en si, eso es importante._ aclaró con firmeza Cyra. _ Creo que no hace falta mitificar el pasado. Hacían las cosas así... porque eran así. _ se encogió de hombros.


_ Nosotras también lo hacemos así porque es así. _ replicó Ahmya.


_ Y nosotras también cambiaremos algún día, seguramente. _ dijo Carlos. _ Espero. 


Las palabras de Carlos dieron vueltas en su cabeza durante la comida. "Nosotras también cambiaremos", ¿hacia dónde? le gustaría verlo, pensó. Recordó entonces la misteriosa tarea que tenía por la noche, le preguntó sobre ello a las dos. 


_ Me suena que ese laboratorio se encarga de los sistemas de los monitores y las pulseras._ dijo Cyra. _ Creo que una vez cogí una tarea allí. 

Ahmya no averiguó mucho más. La sala dedicada al debate de nivel 3 se parecía a la biblioteca pero era mucho más pequeña. La ventana daba a otro generador de olas sintéticas, había una estantería con algunos libros que las usuarias de la sala dejaban por allí, había tablets, una pizarra con botones y boligrafo electrónico y asientos repartidos. Ella cogió una de las tablets de e-paper para anotar cosas. Poco a poco fueron llegando las participantes, que conversaban entre ellas antes si quiera de comenzar oficialmente la sesión. Cuando llegó el monitor, intercambió algunas impresiones con Ahmnya sobre temas que pretendía sacar si la conversación se apagaba y comenzaron. 

_ Yo tengo una pregunta. _ dijo firmemente un chico de unos 14 años. 


_ Compártela si quieres, Kamyar (کامیار). _ respondió el monitor.


_ Desde hace generaciones que estudiamos el pensamiento artificial, para ayudarnos con tareas, ¿no? pero siempre buscamos lateralmente que sean cada vez más independientes. Mi abuelo investigaba sobre eso y siempre estaba obsesionado con que el pensamiento artificial en el que trabajaba pudiera crear pinturas. ¿Qué es más importante, que creen cosas o que comprendan cosas? 

_ Mi abuelo también trabajó con eso de joven. _ aportó una niña de más o menos la misma edad. _ Pero esos pensamientos (creo que él lo llamaba inteligencia) también necesitan comprender para crear, creo que la pregunta no tiene sentido.

_ Pero no comprenden, sólo se aprenden patrones. _ incidió de nuevo el niño. _ Si tuvieramos que diseñar un pensamiento como el que hacía mi abuelo, ¿tiene sentido si no puede reflexionar sobre otras obras?

_ Quizás depende del objetivo._ se encogió de hombros una tercera niña. 


_ ¿Puedes argumentar un poco más, Ariadna (Ἀριάδνη)? _ intercedió el monitor. 


Ahmya observaba atenta, completamente absorta en la discusión de las niñas.

_ Depende del objetivo. Si quieres que ese pensamiento cree una pintura que te haga recordar un paisaje concreto, se puede conseguir eso sin necesidad de algo más complejo. 


_ aclaró Ariadna. _  Si quieres un pensamiento que te ayude a identificar patrones de un estilo de pintura concreto en obras ya hechas... quizás sea el caso que plantea Kamyar. 


Las niñas se miraron entre ellas reflexionando sobre esto. El monitor, por su parte, miró a Ahmya, esperando su intervención. Ella sobresaltada recordó su cometido en la sala y revisó sus notas.

_ Habéis hablado de los objetivos del pensamiento artificial. _ comenzó Ahmya. _ ¿Por qué no hablamos sobre los recursos que necesita?


Las niñas hablaron largo y tendido del tema; sobre sus propias experiencias, sobre cosas que habían leído, que habían escuchado. 

Los siguientes días, tal y como le había contado a Cyra y Carlos, eligió mayoritariamente actividades de jardín, huerto y cuidado ambiental. Pero en el monitor siempre estaba de fondo la tarea de "testeo de actualizaciones sobre el sistema automático de guiado personal " cada vez a diferentes horas, así que movida por la curiosidad, decidió apuntarse un día que el horario le venía bien. El laboratorio estaba cerca de la estación de radio del edificio, que compartían con otras dos comunidades cercanas. Aunque era por la tarde casi de noche y muchas ya habían dejado el área de trabajo, en esa zona aún había algo de actividad, gente que entraba y salía de los laboratorios o la estación. En una de las puertas había un cartelito de e-paper que de vez en cuando se fundía y volvía a cargar el mismo mensaje.

"testeo de actualizaciones"

Entró suavemente por si interrumpía algo y solo encontró a cuatro personas enfrente de unos ordenadores hablando tranquilmente. Al verla, la invitaron a acercarse. 

_ Estamos intentando diseñar un sistema nuevo para recomendar tareas y gestionar el trabajo de la comunidad. _ explicó una de ellas. _ Pero es un proceso delicado que no puede hacerse de un día para otro, además es difícil saber si funciona bien o no antes de lanzarlo.

_ Estamos intentando hacer pruebas a grupos pequeños de voluntarios para que nos cuenten si les parece que funciona bien. Estamos en una fase muy precaria, pero en teoría, es funcional. _ aclaró otro.


_ ¿Cómo puedo ayudar yo? _ Se interesó Ahmya.


_ Sólo tendrías que dejarnos actualizar tu pulsera. No te preocupes, luego puedes volver a instalarte el sistema de siempre, es temporal. Luego tendrías que decirnos qué te ha parecido. _ Respondió la primera. Las otras dos restantes que aún no habían dicho nada, parecían estar preparando el ordenador para instalar el sistema.


Ahmya se quitó la pulsera y se la entregó a la mujer que le estaba explicando todo. No tenía especial apego por el aparato, simplemente era útil. Observaba atentamente el ordenador, aunque no supiera que estaban haciendo exactamente, pero de ahí saldrían los cambios por los que tanta curiosidad tenía. 

_ ¿Dónde puedo leer los detalles del proyecto?_ preguntó. 


_ Todo está en el repositorio del departamento de pensamiento artificial. _ Aclaró uno de ellas. _ Ven mira, puedes tomar nota del código del proyecto.


Ahmya se acercó y anotó en la tablet que llevaba encima el código. Miraría el  proyecto en su habitación, tranquilamente. Mientras terminaba de anotar los detalles, le devolvieron su pulsera.

_ Gracias, esperamos noticias tuyas en unos días._ dijo la mujer que se lo entregó._ Si tienes algún problema ven a este mismo laboratorio. 


Una vez en su habitación, encendió su ordenador y entró en los repositorios del departamento a buscar el proyecto que le habían indicado. Como otros proyectos que afectaban al día a día de la comunidad, necesitaba la aprobación de una representante de cada departamento para que se _mergease_ en la rama principal¹. El repositorio estaba dividido en el código, los datos para entrenar el pensamiento artificial, y una breve documentación. Por gesto tradicional, una especie de respeto a la tradición de años atrás, la licencia libre también estaba entre los documentos subidos. 

El código parecía bastante normal. Abrió su terminal para compilarlo, no hubo ningún problema. Necesitaba la información de una pulsera para funcionar, pero el repositorio venía con un _script_² que generaba "pulseras" emuladas aleatorias. Estuvo jugando un rato con el programa, comprobando las decisiones de tareas según la pulsera, pero realmente tampoco comprendía muy bien la decisión tomada. Analizó los datos de la pulsera que usaba el programa: por un lado, acumulaba la información de las tareas elegidas durante dos meses atrás; por otro lado analizaba las pulsaciones de ese mismo momento y esa misma hora 24 horas antes; la hora y fecha. Evidentemente no guardaba información personal de ningún tipo. Las pulseras se reseteaban y cambiaban constantemente para repararlas, y no tenía sentido relacionar esos datos con las personas que las llevaban.

Al día siguiente se acercó al monitor con cierta expectación. No sabía que esperar, pero estaba ilusionada en cualquier caso. El monitor tardó un poco más en procesar la información pero le devolvió finalmente una serie de resultados en verde. 

"Cuidado huerto ala Norte de 10:00 a 14:00", "cuidado del huerto ala Este de 10:00 a 14:00", "asistencia en la cafetería de 15:00 a 17:30", "limpieza del jardín para educación". 

De algún modo se sintió decepcionada. Tampoco sabía que esperar pero no encontró mucho cambio. Le entró curiosidad sobre la decisión de la cafetería, así que eligió esa, además del huerto del ala Este. Algunos días decidía irse de visita a otras comunidades, para saciar una mezcla de curiosidad y practicidad. Hace poco había sugerido a través del repositorio de eduación la posibilidad de intercambiar miembros de una comunidad a otra durante algunos días con la intención de cambiar de aires y aprender nuevas dinámicas. Sin embargo, dado que estaba experimentando con el sistema nuevo, esa semana sólo se pasó por otra comunidad en calidad de visitante. En uno de los huertos de una comunidad vecina solía elegir tareas Minami (南), irónicamente en una comunidad al norte de la ciudad³. Las ciudades estaban dedicadas a los parques y zonas comunes entre comunidades en sus extremos. De hecho la emisora de radio de su comunidad estaba por eso mismo, pegando al límite donde comienza una de esas zonas comunes.

Minami fue una de las primeras en probar el sistema de intercambio de comunidades, y desde entonces ella y Ahmya habían mantenido contacto, especialmente por su afición a las plantas. Ahmya se la encontró cuidando de unas flores en el jardín escolar de su comunidad. Minami le explicó que necesitaba despejarse un poco de las tareas más exigentes de los huertos principales y había estado buscando pequeñas tareas de ese estilo hasta recuperarse.

Minami señaló a Ahmya;

"¿Has vuelto a pedir otro intercambio de comunidad unos días?" _ signos.

_ No, no he pedido otro intercambio. Tan sólo estoy de visita, me ofrecí a probar un sistema nuevo de recomendación de tareas, y debo quedarme unos días en la misma comunidad.


Mientras hablaba con la voz, Ahmya también hablaba con las manos para su amiga. Ella asintió, y mostró su interés en la pulsera. Ahmya se la acercó, pero tras analizarla a Minami no le resultó muy emocionante, y tampoco estaba muy equivocada (pensó Ahmya) pues no había notado nada ni mejor ni peor en la toma de decisiones del sistema. Ambas dieron un paseo por el parque que comunicaba sus zonas. La chica tenía una forma de ser extrañamente directa que calmaba a Ahmya, su transparencia y su orden eran la antitesis de Cyra. A petición de Minami caminaron de la mano un largo trecho hasta casi el final de parque, donde se despidieron.

A la mañana siguiente las sugerencias del monitor extrañaron a Ahmya. Tan sólo señalaba en verde una de las tareas ambientales, el resto de tareas eran de asistencia de varios tipos. Deseleccionó las sugerencias para comprobar si es que no había en general tareas de esa índole, pero si que había, simplemente no se las sugería. Le sorprendió bastante pero, llevada por la curiosidad de este repentino cambio, decidió seguir el consejo del sistema y se apuntó a tareas de asistencia de limpieza. 

Aunque ya había pasado una semana, decidió probar una semana más porque realmente no sabía bien que decirles a las del departamento de pensamiento artificial. Si tuviera que adivinar, diría que era completamente aleatorio. Durante otro par de días sus tareas fueron variadas y no parecían tener un camino claro, ¿Estaría roto? se lo planteó realmente. 

En su habitación, encendió el ordenador y abrió la carpeta con el experimento que estuvo probando para comprender como funcionaba. Nada le dio pistas del por qué de las tareas.

Decidió olvidarse de ello un rato para leer noticias de las diferentes comunidades, y de otras ciudades. Aunque intentaba olvidarse del tema, cada pequeña cosa que leía en las noticias le recordaba al pequeño experimento en el que estaba participando. "Aumentan el número de interesados en electrónica ambiental en la comunidad de cristal", de cristal porque tiene un enorme salón común de cristaleras que llamó mucho la atención cuando se abrió, aunque ahora sea un diseño normal. "¿Estarán usando un programa nuevo de sugerencias, también?" se preguntaba. Y tras un buen rato llegó a la conclusión de que quizás las extrañas sugerencias que recibía en su comunidad le parecían así porque el pensamiento artificial tenía una visión global que ella no alcanzaba a ver. Asintió para sí, esa era la explicación más convincente. 


¿Quiénes eran las otras testeadoras del sistema? La chica se lo preguntó mientras organizaba libros en la biblioteca, su primera actividad ese día. Quizás tuviesen la misma duda que ella, o a lo mejor incluso alguna conocía la respuesta con certeza. Mientras colocaba los libros podía oír el suave y lejano ruido del rompeolas artificial que pocos días antes la había distraído de su lectura. Se sintió extrañamente en calma, no sabía bien por qué. Había parte de ella que se sentía liberada mientras colocaba los libros, tenía la sensación de olvidar algo pero no darle importancia. Una cara familiar le sacó de su meditación. 

_ Hey _ Cyra le sonreía desde el final de la estantería que estaba organizando. Se acercó un poco más. _ Por fin te veo. _ le dijo casi entre susurro para no molestar al resto. _ ¿tomamos algo cuando termines? 


_ Ah, claro. _ Ahmya parecía recién despierta. Hacía unos días que no se veían. _ Dentro de media hora. 


Cyra asintió y le dio un suave apretón en el brazo a Ahmya antes de irse. Se recordeó en esa sensación un momento antes de seguir su trabajo, le gustaban las muestras de cariño de Cyra. Suspiró como quien se obliga a aferrarse al presente y continuó colocando y organizando; hacia rato que había dejado de pensar, solo trabajaba. 

El paso de la biblioteca al pasillo era como salir del agua y recibir de golpe todos los sonidos que antes eran murmullos. Entre el barullo encontró a Cyra, que gesticulaba para que la siguiese. Las chicas fueron juntas a la cafetería, hablaron de aquellos últimos días que no se habían visto. 

_ Las niñas ya han empezado a montar los sensores del huerto. _ contaba Cyra. _ Aunque uno de ellos empezó a fallar y dos de ellas discutieron, así que tuve que levar más. _ se encogió de hombros. 


Pensó en lo lejano que le resultaba el recuerdo de preparar los kits para el huerto escolar. De pronto esa sensación de tranquilidad y seguridad que había alcanzado hace un momento se convirtió en inseguridad y se levantó apresuradamente. 

_ Tengo que comenzar el siguiente turno. _ De excusó Ahmya. Hizo un gesto de despedida antes de irse abruptamente, Cyra no tuvo tiempo de reaccionar. Mientras se dirigía a su cuarto, pues no tenía tareas hasta una hora después pese haberle dicho eso a la chica, pensó en que quería sentir otra vez esa sensación de tranquilidad, que no sabía exactamente a qué se debía. Tumbada en la cama pensó que quizás debería coger librería de nuevo al día siguiente y así lo hizo, pese a que las recomendaciones indicaran otra dirección de tareas. Sin embargo no consiguió sentirse así, y se rindió de nuevo a la elección del nuevo programa, que está vez la llevó de nuevo a cuidados de jardín. Pasaron varios días en los que poco a poco dejó de pensar en las tareas que hacía para pensar en perseguir esa sensación que notaba de vez en cuando sin darse cuenta. Cuanto más investigaba como sentirse así, más forzado y complicado le resultaba, y sin darse cuando habían pasado varios días sin mantener una conversación pausada con ninguna de sus allegadas. Se dio cuenta mientras preparaba la sala para la hora de debate de infantil, siguiendo las notas de la monitora especialista, decidió ir a buscar a Cyra esa misma tarde, además sentía que le debía una disculpa de su último encuentro. 


A la salida de su tarea, se dirigió al laboratorio de electrónica, donde se encontró con Carlos. 

_ Cyra... Ahora que lo dices hace un par de días que no viene por aquí, asumía que se había tomado un descanso para investigar e otro departamento. 


A Ahmya le extrañó, pero Carlos no supo decirle exactamente donde estaba. La chica dedicó el resto de la tarde a buscar a Cyra, sin éxito. ¿Habría pedido un intercambio? No podía saberlo. Al final de la tarde se cruzó con una de las investigadoras de pensamiento artificial, su conversación derivó en que ya podía devolver la pulsera y comentar los resultados, se la llevó al laboratorio donde respondió algunas preguntas y le dieron una pulsera reprogramada normal, aunque Ahmya solo pensaba en su amiga. 

_ ¿No habrá venido por aquí a investigar temporalmente una chica nueva, más o menos así de alta, de ojos oscuros, morena de pelo rizado? _ pregunto Ahmya antes de irse, colocando su mano unos centímetros por encima de su propia cabeza. 

Las investigadoras se miraron en silencio un segundo hasta que una de ellas negó con la cabeza suavemente. Ahmya, decepcionada, se fue a su habitación a leer un rato, y decidir su próximo movimiento. Cuando estaba ya casi dormida y decidiendo acostarse, alguien llamó a su puerta. Con gesto intranquilo, una de las investigadoras del grupo de pensamiento artificial le pidió permiso para entrar.

_ No debería decirte esto. _ empezó mientras Ahmya le cedió el paso al interior y cerraba la puerta tras de sí. _ Esa chica si que estuvo en el laboratorio, no quería mentirte, parecías preocupada. Es del departamento de electrónica, ¿verdad? 


Ahmya asintió en silencio mientras se sentaba en su cama. 

_ Vino mostrando interés en la pulsera, no sabía por qué, aunque ahora deduzco que se lo dirías tú. Insistió en analizar el microcontrolador. _ se dio cuenta de que la investigadora no llevaba su pulsera. _ Y... Bueno le dieron una de las que tenían guardadas pero el caso... El caso es que no eran las del experimento. 


_ Pero... _ empezó Ahmya _ Pero las pulseras son iguales, ¿no? Lo único que cambia es el firmware. 


La investigadora miro hacia otro lado y negó lentamente con la cabeza. 

_ No, eso es lo que te dijimos... Pero no. Y el programa del repositorio público también es una tapadera. Te dimos el cambiazo, el hardware es distinto. Escucha _ la chica se acercó a Ahmya _ No se donde está tu amiga pero creo que puede estar metida en algún problema y desde que has venido tú tengo algo de miedo. 


_ ¿Y cómo se que me dices la verdad? ¿Y tu pulsera? _ Ahmya se levantó, no sabía si sentirse frustrada, enfadada, asustada... 


_ Nos obligan a llevar pulseras nuevas... Y llevan un sensor de geolocalización. Le di una de verdad a esa chica en privado, y me quedé con otra, aunque la he dejado en mi cuarto. No se si habrán notado que falta una... Y no quería que se supiera que estoy aquí. No puedo demostrarte que te digo la verdad, pero quería hacer lo correcto. 


Le temblaba la voz al decir estas últimas palabras. Se despidió abruptamente repitiendo que no quería que la vieran y salió rápidamente antes de que Ahmya pudiera comprender lo que pasaba. Tras encontrarse a solas con el silencio de la noche, sólo podía notar sus propios latidos. De pronto, la pulsera en su muñeca le pesaba. No había cambiado ni un poco desde esa misma mañana, pero ya no la notaba igual. Se la quitó y la dejó sobre la mesa. 

Nadie sabía nada de Cyra, Ahmya estaba enfadada. ¿Cómo era posible? Siempre estaba rodeada de personas. Quería llorar de rabia. Bajo este sentimiento empezó a correr camino al laboratorio, pero a medio camino pensó que podía comprometer a la investigadora que le contó todo y aminoró el paso con la intención de calmarse. Este brusco cambió de ritmo rompió el último pedazo que sostenía sus emociones y se echo a llorar en mitad del pasillo en el que se había detenido. La ente la observaba, algunas pasaban corriendo porque iban a sus respectivas tareas, otras no sabían si decirle algo, nadie se fijo en que no llevaba pulsera. 

Finalmente un chico se acercó y se ofreció a acompañarla a por agua, y caminaron lentamente hasta la cafetería más cercana, donde una vez que ella dejó de llorar y tenía un té de máquina entre las manos, se separaron. Miró el fondo de su vaso, perdida. ¿Era buena idea presentarse así en  laboratorio? Sabrían que no estaba haciendo sus tareas ese día, quizás. No tenía mucho tiempo antes de que se percatasen de que era algo más que un día de descanso. Levantó la mirada bruscamente, ya sabía qué hacer. Salió corriendo al laboratorio de electrónica, aun estaban trabajando, se acercó a Carlos, que manipulaba un PLC de control de robots limpiadores. 

_ Ahmya, que sorpresa. _ dijo al verla. _ ¿quieres ver mi robot? Mira, cuando recibe... 
_ Carlos necesito tu ayuda. _ cortó rápidamente Ahmya. _ se qué estás liado pero ¿podrías coger tus herramientas y venir conmigo? Es urgente. 


Dado que Ahmya no solía mostrar ese temperamento, el chico se sintió aturdido, pero asintió, recogió sus cosas y siguió a Ahmya. Ella le guió hasta su cuarto, una vez dentro cogió la mochila de Carlos y sacó el kit para desmontar dispositivos pequeños, bajo la atenta mirada del chico, Ahmya desmontó la pulsera, ambos estaban en silencio. Una vez estuvo abierta, Carlos la observo por encima del hombro de la chica. 

_ ¿Qué estás haciendo? _preguntó él. Ella rebuscó de nuevo en la mochila, tras pedirle permiso a Carlos, y sacó una tercera mano con lupa, en la que colocó la placa. Ella lo observó a través de la lupa un momento. 


_ ¿Crees que este componente debería estar aquí? _ preguntó al final, señalando con una pinza un compenente de la placa, parecía una antena cuadrada. Carlos lo miró detenidamente a través de la lupa. 


_ Pero... No puede ser. _ se dijo para sí. Miró a Ahmya. _¿Que es esto, qué está pasando? ¿Por qué tiene GPS? 


_ Creo que Cyra puede estar en problemas por culpa de esto. _ respondió mientras mira la placa recostada en su silla. _ ¿Crees que podemos averiguar algo más sólo con esto? ¿Sacar el firmware, averiguar algo más? 


Carlos pensó un momento, y sacó el ordenador, JTAG, soldador y otras herramientas de su mochila. 

_ Bueno, vamos a averiguarlo. 


Tras horas probando cuidadosamente sobre el dispositivo, consiguieron sacar algunos resultados prometedores. Ahmya decidió quedarse al ordenador rebuscando mientras Carlos se toma a un descanso, se preparó un café entre suspiros cansados. Finalmente consiguió dumpear la RAM y obtener información sensible, algunas contraseñas que no sabía para qué eran. También la memoria flash, de donde extrajo finalmente el firmware. 

_ Vaya lío. _ comentó Carlos sentado al lado de Ahmya obervsando los directorios extraídos. Los scripts guardados indicaban claramente que los datos vitales y de posición se mandaban a algún servidor, y que la toma de decisiones era o bien aleatoria o bien controlada a distancia. 


Ahmya sintió rabia y decepción. 

_ Si conseguimos acceso al servidor puede que encontremos una forma de encontrar a Cyra. _ dijo mirando la pantalla. _ Pero no se bien cómo hacerlo, ese tipo de intervención no entra dentro de las cosas que nos enseñaban en mantenimiento. 


Carlos negó también con la cabeza cuando Ahmya le miró interrogante. Finalmente decidieron rebuscar entre los archivos de repositorios antiguos, que aún flotaban en la Red distribuída, a modo casi de museo digital. La mayoría de esos programas formaban parte de una época de transición, en la que el software libre comenzó a ganar importancia pero la tecnología perdió protagonismo para convertirse en un apoyo a la organización de comunidades. Poco a poco la necesidad de construir de cero cuando todo lo demás comenzó a derrumbarse, hizo que el software libre fuese la única vía para mantener un ritmo decente en la evolución de la tecnología, pero las necesidades fundamentales de educación, comida, limpieza y otras necesitaron muchísima más atención. Poco a poco comenzaron a fundirse todas; las disciplinas dejaron de tener sentido individual, también se desatendieron las actividades que simulaban ataques o gestionaban la violencia de forma desordenada. Como todo, esto trajo ventajas y problemas, pero en cualquier caso, Carlos y Ahmya podían seguir rebuscando entre los archivos antiguos, de tiempos lejanos, en busca de una respuesta. Se hicieron con una colección de programas antiguos que podían ayudarles en su tarea y trabajaron toda la noche intentando adaptar aquellas reliquias digitales a sus necesidades, hasta que finalmente consiguieron acceder al servidor. Decidieron que sus siguientes pasos iban a requerir cuidado y detallismo así que pensaron dormir un rato antes de continuar. Tumbadas en la cama, Ahmya reparó en todo el desorden que había sobre la mesa, todas las herramientas esparcidas, pantalla parpadeante y la tercera mano sosteniendo los restos del dispositivo. Se durmió imaginando tiempos pasados, cuando su amigo ya estaba más que inmerso en su descanso. 

Carlos la despertó  horas después y se pusieron a trabajar juntas en la segunda parte de su plan. Descubrir la localización de Cyra. 

El laboratorio de pensamiento artificial estaba tranquilo ese día, pero Clara, la investigadora que había alertado a Ahmya del peligro, estaba alerta e inquieta. Mientras intentaba ocultar su inquietud, reparó en su monitor de control en un aviso de alerta en el servidor. Rápidamente sospechó lo que ocurría y eliminó la alerta, cubriendo el fallo con la máxima discreción. 

_ Me ha parecido ver un aviso de algo. _ comentó otro del equipo que andaba mirando los avisos en su propia pantalla. 

_ Era un tema de configuración por defecto, ya está arreglado. _ respondió Clara lo más neutral que pudo. Su compañero se encogió de hombros y siguió trabajando. La chica retuvo un suspiro de alivio, y cruzó los dedos para que, desde el otro lado de la pantalla, consiguieran su objetivo. 


Ahmya, con un mapa de la ciudad, dibujaba un círculo de acción con las localizaciones que finalmente habían descubierto. Le costó mucho abstraer una zona en concreto, pero finalmente creó un perímetro. Al analizarlo, tuvo una intuición. Carlos se quedó investigando en el ordenador mientras Ahmya salía de la comunidad. 

Minami había vuelto a sus tareas habituales, había recuperado fuerzas y arrastraba un saco lleno de tierra para preparar algunas semillas de temporada. Estaba tan concentrada que casi no se percató de Ahmya hasta que estuvo muy cerca. Soltó el saco sonriendo. 

"Hola de nuevo, ¿como estás?" - signos

"La verdad no estoy en mi mejor momento. Tengo que preguntarte algo." - signos. Estaba tan nerviosa que se le olvidó hablar con la voz mientras hablaba con las manos. Los gestos casi se le atropellaban.

"Calma, despacio. ¿Que ocurre?" - signos. Minami se quitó los guantes de trabajo y acarició el brazo de Ahmya, intentando tranquilizarla. Ahmya suspiro profundamente. 

"¿Te acuerdas de Cyra?¿La has visto últimamente?" - signos. Al deletrear su nombre con las manos sintió que cada segundo que seguía desaparecía le pesaba. 

" Si, estuvo aquí hace poco. Hace unos cuatro días. Quería saber algo del departamento de pensamiento artificial de esta comunidad. Y de los horarios de la estación de radio." - signos. Minami estaba preocupada. Preguntó si Cyra estaba bien. 

" No lo se. Ha desaparecido. " - signos. Ahmya se llevó las manos a la cara, estaba visiblemente agobiada. Minami le abrazó con fuerza hasta que pudo sentirse mejor. Se separaron suavemente. 

" Gracias por tu ayuda. Voy a seguir buscando." - signos. Ahmya corrió hacia la estación de radio de la zona. Estaba resuelta a resolver aquello de una vez. 


La estación estaba ajetreada, tres de las encargadas estaban revisando sus notas para la programación de la tarde. Intercambiaban apuntes y practicaban algunas partes. Todo el barullo paró cuando abrupta ente entro Ahmya, jadeante, y las miró de arriba a abajo. 

_ ¿Ha pasado por aquí una chica alta, morena, de pelo rizado y ojos oscuros? _ pregunto Ahmya, aunque sonaba a una orden. 


Ellas se miraron un momento, negaban con la cabeza. 

_ No ha venido nadie hoy, estábamos organizando la programación para después, no tenemos ninguna entrevista planeada. ¿Por qué iba a venir esa chica? Si quiere decir algo podemos encajarlo e... _ la encargada no acabó la frase porque, tan abruptamente como Ahmya entró, salió de la emisora. 


Tenía que estar por los alrededores, o más bien deseó que lo estuviese. Rebuscó por el mismo parque por el que había caminado con Minami unos días antes, pero mientras lo hacía comprendió que quizás no encontraría a Cyra en algún sitio visible. Revisó el mapa que había triangulado con Carlos y miró a su alrededor. Vio que parte de la zona estaba cerrada por obras y se acercó a mirar, a parando las ramas crujientes de una zona más descuidada, con cuidado de no tropezarse. Finalmente le pareció ver a su amiga sentada en el suelo, su corazón se congeló unos segundos antes de correr hacia ella. Según se acercaba pudo ver que estaba atrapada con algo entre las manos, cuando llegó hasta ella, Cyra parecía confundida, con las mejillas húmedas y mirada cansada preguntaba entre sollozos qué hacía Ahmya allí. Ahmya también lloro suavemente mientras agarraba su cara, justo antes de mirar sus manos, y comprobar que tenía una especie de de aros metálicos alrededor de sus muñecas que mantenían a la chica agarrada a un poste metálico, eran unas esposas pero Ahmya no las había visto nunca. Intentó forzarlas a mano sin éxito, la rabia que había sentido antes volvió. Se levantó y empezó a patear con fuerza el alambre que conectaba los dos aros. Desde que eran pequeñas nunca experimentaban esa clase de violencia, gestionaban las emociones de otra manera, y de alguna forma esa experiencia era a la vez aterradora pero natural. Empezó a jadear mientras pateaba, y su amiga apartó la vista e intentaba tensar el cable para ayudar a romperlo. Las esposas parecían antiguas, finalmente cedieron, se rompieron. Ahmya casi se tropieza al parar bruscamente de patear, recuperó el aliento unos segundos antes de ayudar a Cyra a levantarse para salir de allí. 

Minami estaba inquieta en su habitación, pero dejó de pensar en ello cuando llamaron con prisas a su puerta. Ahmya entró con Cyra tambaleante, se sentaron en el suelo de la entrada mientras Minami cerró la puerta y con prisas corría a por su botiquín. A los pocos segundos volvió con una bolsa para limpiar las heridas de las muñecas de Cyra, que aún tenían las anillas metálicas contando. 

_ Lo siento Minami, gracias por la ayuda. _ dijo Ahmya signos torpes y voz quebrada. 


"Gracias." dijo Cyra con signos mientras retenía las lágrimas. Minami le dio unos golpecitos suaves en el brazo para calmar a la chica, acto seguido le dio el botiquín a Ahmya que relevó la tarea cuidadosamente. 

_ Tengo información sobre el dispositivo, en mi cuenta. _ Dijo con voz y signos. Minami asintió, se levantó y encendió el ordenador, rápidamente abrió una terminal. 


_ Si lo publicamos en el repositorio ahora sabrán que has escapado. _ respondió Ahmya con signos y voz para las dos, cuando soltó el algodón y el alcohol, y Minami dejó de mirar la terminal. _ Y además puede que nos bloqueen el acceso al servidor. Tengo una idea. 


Carlos miraba cuidadosamente el servidor, de vez en cuando observaba de reojo el dispositivo desmontado en la mesa. El monitor del ordenador mostró un aviso que lo sobresaltó, su corazón se aceleró al ver que era un mensaje de Ahmya. 

" Encontrada. Todo bien. ¿Crees que podrías romperlo?" 

Carlos suspiro. "Pues vamos" suspiró. 

Clara intentó mirar algo en la terminal pero falló la contraseña. Pensó que era por los nervios así que lo intentó otra vez, y otra, pero no funcionó. De pronto se dio cuenta que ya no tenía la contraseña. Miró a su alrededor cautelosa ente y se fijo que nadie estaba usando la terminal para acceder al servidor, así que dedujo que el cambio era de fuera. Se apartó lentamente del teclado. 

_ Voy a por café, vengo después. _ dijo con normalidad. En el fondo tenía prisa pero intentó no reflejarlo, una vez fuera del laboratorio suspiró y aceleró el paso poco a poco hasta salir corriendo. No volvió a esa comunidad si quiera. 

Tras cambiar la contraseña de las usuarias, Carlos comenzó a bloquear todo, copiarlo en su ordenador y borrar todos los datos sensibles. Una vez su trabajo estaba acabado y limpió su rastro, escribió a Ahmya. 

"Todo listo." 

Ahmya hizo un gesto de aprobación a Cyra y Minami tras ver el mensaje, y Cyra se dispuso a publicar toda la información en el repositorio. En pocos segundos, toda la información estaba pública, y algunas representantes de laboratorio ya lo habían leído. Las tres miraban atentamente la pantalla, algunas sujetos de prueba del experimento empezaron a replicar las pruebas y apoyar la publicación de Cyra. En cuestión de minutos, las responsables de pensamiento artificial de la comunidad de Cyra y Ahmya fueron bloqueadas y vetada de todos los proyectos de tecnología. Un equipo de seguridad fue rápidamente asignado a Cyra y a las otras testeadoras del experimento, incluyendo a Ahmya. Las dos dos se miraron y sonrieron, Minami suspiró. 

Carlos guardó los restos del experimento en su mochila, recogió y salió de la habitación cuidadosamente. 

Mientras Ahmya miraba la pantalla y las notificaciones saltar, recordó de pronto las olas artificiales de la biblioteca. Nunca llegó a terminar su lectura, pensó. Ella pensaba que el antiguo y el nuevo mundo distaba tanto que nunca lo comprendería aunque leyese sobre él. Pero entonces lo supo, no hay un antiguo y un nuevo mundo, solo un mundo. 








1) aquí va explicacion sobre merge request

2) aquí va explicación de script

3) Minami significa "de la zona del sur" en japonés, es un nombre de mujer. 
