# Relatos

Este repositorio es una lista de todos los relatos grandes y pequeños en español o inglés que vaya escribiendo. Algunos de ellos forman parte de la editorial de Post Apocalipsis Nau y pueden escucharse en el podcast que se publica en el Salto cada 15 días. Otros no tinen un contexto fijo.

Algunos relatos se ciernen entorno a un universo inventado en Marte, que acumulo en la carpeta de [universo_marte](). 

Si quieres arreglar o corregir o proponer algún cambio no dudes en hacer Merge Request o abrir un issue en este mismo repositorio, será todo bienvenido.



## FAQ

Aquí resumo algunas preguntas que me han hecho diferentes lectoras a lo largo del tiempo sobre los relatos.

**¿Cuando hablas en plural femenino quiere decir que sólo hablas de mujeres?**

No, hablo en plural en general. Me gusta leer estas historias con ese recurso, pero te animo a que al leer, comprendas plural general donde se incluya el masculino, femenino y no binario. 

**¿En qué año se desarrolla la historia?**

En ninguno en particular, ¿siguen contando los años del mismo modo? no es importante. En los relatos ese dato en particular no importa, sólo que son en el futuro o en un universo alternativo. 

**¿Vas a traducir los relatos?**

Mi idea es poder ofrecerlos indistintamente, pero es un proyecto más a largo plazo. A veces escribo en inglés y a veces en español porque comienzo las historias en base a notas que hago en momentos del día, y depende de lo que esté estudiando o leyendo estoy usando uno u otro. Si alguien tiene interés en traducir por algún motivo antes de que yo lo haga, puede sugerir un merge request y lo agradeceré un montón. Por ahora no es un proyecto estable.


