Everything seems fine, except from the line:

 val = map(val, 0, 1023, 0, 160);

Which should be:

 val = map(val, 0, 1023, 0, 180);


Why? You are trying to scalate the potentiometer value to 180 degrees (from left to right) not 160 degrees. when scalating to the wrong degree-values, the maximum goes from 180 to 160 making the control weird.

