When the grandparents of Ylla arrived in Mars for the first time, it was supposed to be a big thing. A very big thing.

Dreaming about Mars colonies was (for some reason she wouldn't understand very well) a very old characteristic of humanity. Since she was born, Ylla was told she carried a huge responsibility on the future of humanity, Earth and Mars. She would look through the thick windows, to the background their parents called "red" or "carmine", they mentioned it was (although beautiful) a bit discomforting. Apparently Earth was mainly blue or colourful but Ylla found that slightly agoraphobic. Mars colonies soothed her, made her feel warmth. She couldn't imagine herself in a mainly blue ("that sounds cold", she though) open space without feeling a bit scared. But she kind of liked that scary feeling for the unknown an yet constantly present planet. She wanted to learn more about it, understand why their grandparents were so obsessed with it.

The colonies worked pretty easily. Everything was already there when their grandparents arrived: The tech was placed beforehand. It automated the plants, the air filtering, the studies: everything. Watching historical documents and videos Ylla found a lot of things she wouldn't understand: people doing the jobs of the automated machines even though the tech was pretty well advanced, people fighting and "killing" each other. But she also found about art, protests and even "memes" which she couldn't possibly understand despite the descriptions. They looked a bit like her, but not quite, they seemed to be their siblings but they weren't. 

She decided to focus her energy into understanding this weird culture, and preserving it somehow far away from the Earth. It was apparently a very important task and a huge responsibility she constantly doubted she could do (as she wasn't born there or even remotely visited). Her first steps, she decided, was to find a pen-pal in Earth. It was difficult as she didn't know anyone there, and the messages would travel slowly, so getting in touch wasn't that easy. 

After a few days her grandmother found someone for her, it was family of a friend she had on Earth. Their name was "Hyu", Ylla was excited to write to them. 

"Hello Hyu" she started "My name is Ylla, I live in Mars. I've heard a lot about your planet. I'm curious about how things work around there"

She mentioned her Mars favourite places to paint, the museum where they had the Perseverance robot, the food... She would also ask a lot of questions about Earth, hoping to better understand it through Hyu. 

"Hi Ylla, nice to meet you" replied Hyu a few days later. "I'm so excited to hear from you. Never talked to someone living in Mars. You are described in here as children tales. Today I looked at the sky and unfortunately it wasn't blue: it was a grey. There are many cloudy days where I live, sometimes it rains, does it rains in Mars? Anyway there are blue days as well. But red? oh no! it would be so weird and scary to be honest."

Ylla though carefully about that. "scary, eh" she said out loud to herself, She guessed that, if Mars suddenly looked green she would freak out too.

"I'm impressed about the technology you explained in your letter" Hyu continued in the letter. "In the place I live at, we have some technology in the public library (where I'm writing to you) we all learn the basics of programming and building, but we don't actually rely that much on tech. We are mostly focused in our greenhouses and parks. As I though you might want to see them, I attached some pictures for you. We actually used to have similar tech you know? but we don't anymore. We chose not to."

Ylla opened the pictures and gasped. It looked amazing! It looked like the greenhouses they have at Mars but bigger, and the parks? similar but... open on the air. She felt like that was dangerous, but she knew Earth had air they could breath. 

After that, she asked for more pictures, more information, even videos. Hyu would record videos of themself while explaining stuff, it was fun and interesting for Ylla. She started painting, drawing and sketching things she couldn't reach with her hand but Hyu sent to her (and she would send those to them, who highly appreciated the gifts). Their friendship would be one of a kind, one of the first that defined what would be a great art branch in Mars: The Humanity Exploration. 
