The master's notes:


Initial classes:

Artistic classes:
- Humanity exploration
- Martian exploration

Science:
- Maintenance engineer
- Robotics
- Biologist
- Herbalist


Humanity exploration

They are mostly into Earth history, and can easily relate with the past they didn't live. They must know at least two Earth languages + Martian English. They must choose a creative medium to specialize at, although they can manage to know more than one. 


Martian exploration

They are "rebels" in the art academia. They intend to create the most different art from the Earth tradition as possible. They probably know one Earth language to understand what they are opposing to, apart from Martian English. They also must choose a creative medium to specialize at, better if it's strongly Martian (such as sculpture with Martian dirt). Their disruptive creativity gives them extra points in thinking.


Maintenance engineer

Everyone knows programming, but they know more than that. Good at electronics. Good thing is, as it is a half activity, this class can be mixed with any other partially. They will not have the full points of any but they can grab some of one.

Robotics

Experts in electronics, creative but maybe too enclosed in technology for using that in other things. They know how every robot in the colonies work and if experienced enough, how to build one. They are also experts in communication devices, which is a good companion for Humanity exploration artists who relies on keeping in touch with Earth.

Biologist

Earth creatures are amazing but Martian creatures are yet to be. They are interested in bacteria, the environment of Mars and the huge amount of possibilities for the future. They usually have a helping pet with a connection that no one understands but themselves. They are good companions to Martian Exploration artists because they know a lot about Martian materials.

Herbalists 

Even though most of the greenhouses are automated, herbalists research and discover new ways to make plants grow and feed the colonies. It's considered one of the most ancients abilities, and also a bit of intuition and heart is necessary. They could be quiet or a blast but either way they have a special connection that lets them understand everyone around them better. They have extra points in intuition and knows when a place is safe for life or not pretty easily. 


Points for the players:

Each player have to choose from regular abilities and special abilities. They will have a certain amount of points they need to distribute. Each completed phase of the game means every player will throw a D4 (dice of 4 sides) and earn the points the dice marks. It's also the will of the master to give extra points each phase to (characters and not players): The most brave, The most cautious, The one with the worst luck and The most helpful. 

Regular start points

Being (0) none and (4) the best they could be, each player distribute 25 points between:

Extrovert
Memory
Logic
Physical resistance
Physical strength
Mental resistance
Mental strength 
Ability with hands
Ability with feet
Fast learning

These abilities will make sense of their actions, so someone with 0 physical resistance wouldn't be able to run away easily or someone with no memory won't be able to tell their friends about things they see or heard properly. It's the job of the master to take care of this. They can add this reward point to either regular and class abilities

They must choose 1 defect between:

Bad sleeping (-1 in critical decisions dice throws)
The bad sleeper usually have nightmares or wakes up in the morning. They lack of some energy in the day. Maybe they are strongly trouble by the past, the present or the future. Maybe they keep worrying... 

Angry issues (-1 in communication and empathy dice throws)
Angry characters are not very well adapted to work in teams, they get stressed out when they need to talk to others, or they simply don't agree EVER, it's difficult to talk to them (jeez)

Impatient (-1 in any action throw that is pair -including 10-)
Impatient people tend to mess up, although they may have luck. They might not wait in the queue for their turn but sometimes they act just in time. They... they mostly mess up. 

Craziness (-1 in any action unless it's a 10)
In a very calculated world, chaos always find place. Crazy people usually means disaster but hey, if they are lucky they are VERY lucky.

Ego (-1 in any action that affects other people -even if it's other people and themselves-)
Even if it's the right option, or the common good, egocentrics don't care. They need to be heard, and also to be the centre of attention. They need things done their way. But they are not right an awful amount of times.

Trauma (-1 in actions taken in stressful situations)
They might have a secret in the past that in regular conditions it's not conditioning... But they go blank if they are anxious. It's difficult to handle certain things. They might need help but sometimes they don't ask for it.

Careless (-1 in any action for exploring the environment, judging or watching something closely) 
They look like not caring at all, but sometimes that's dangerous. There are certain times than, loosing information might be critical.





They can get rid of it with a proper development after 2 phases. For example Bad sleeping could go away with a proper herbalist treatment or internal creative peace (among others), angry issues could be worked out with the correct companions and doing good communication choices despite the -1... It's the choice of the master to judge the effort. 

Special abilities for classes:

Humanity Exploration

creativity (+2 in any creative or artistic action)
inspiration (they can throw a dice before a critical decision, if they have a pair number, they get a +1)
history (+1 in any action that depends on knowing about the past)

Martian Exploration

creativity (+2 in any creative or artistic action)
inspiration (they can throw a dice before a critical decision, if they have an odd number, they get a +1)
prediction (+1 in any action that depends on guessing)

Maintenance engineer

electronics (+1 in any electronics related action)
handy (they can throw a dice before a manual action, if it's a pair number, they get +1)

if they mix with....

Humanity/Mars exploration: they get a +1 in any creative or artistic action
Robotics: they get a +1 in robots related actions
Biologist: they get a +1 in material election actions
Herbalist: they get a +1 in environmental election actions


Robotics

electronics (+2 in any electronics related action)
robot friends (+1 in any robot related activity)
communication (They can throw a dice before a communication related activity, if it's odd +1)

Biologist

Materials (+2 in any material election related action)
animal friends (+1 in any action related to animals, including their pets)
nurse (They can throw a dice before a medical judgement action, +1 if it's pair)

Herbalist 

environment (+2 in any environment judgement action)
plant friends (+1 in any plant related action)
empathy (They can throw a dice before any action that requires empathy with any kind of living being -not robots-, +1 if it's odd)


Once the characters are done is the master job to check if abilities and defects are relatable to the characters' background. Look closely as the story makes more sense to all of the participants if the character are well built. 




Required Dices:

10 sides for actions (0 - 9), 0 is 10 (the best possible outcome ever!), 1 is the worst and requires them to throw a 20 sided dice or two 10 sided dices. If the outcome is +15, they keep the 1. If the outcome is -15 they have to lower 1 point in that ability for 3 turns. 

4 sided for ability level up in the end of each phase (phases are divided in the story development).


Other suggestions:

As a master, look for environmental music for some critical or stressful situations to make the game more interesting. Feel free to create characters to help or discomfort the players, but try to adjust them to the level they are working with at that moment. Feel free to suggest classes and abilities to new players.  


